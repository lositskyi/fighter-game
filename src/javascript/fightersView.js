import View from './view';
import FighterView from './fighterView';
import DetailsView from './detailsView'
import { fighterService } from './services/fightersService';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters'});
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    let target = event.target;

    if (target.className != 'fighter' && target.className != 'exit') {
      target = target.parentNode;
      if (target.className != 'fighter') return;
    } else if (target.className == 'exit') {
      target.parentNode.remove();
    }

    if(!this.fightersDetailsMap.has(fighter._id)) {
      const fighterDetails = await fighterService.getFighterDetails(fighter._id);
      this.fightersDetailsMap.set(fighter._id, fighterDetails);
    }

    const details = target.querySelector('.fighterDetails');
    if (!details ) {
      const fighterInfo =this.fightersDetailsMap.get(fighter._id);
      const detailsView = new DetailsView(fighterInfo, this.editFighterInfo.bind(this));
      target.append(detailsView.element);
    } else {
      return;
    }
  }

  editFighterInfo(event) {
    const target = event.target;
    const id = target.parentNode.getAttribute('id');
    const inputs = target.parentNode.getElementsByTagName('input');
    const fighter = this.fightersDetailsMap.get(id);

    [].map.call(inputs, item => {
      fighter[item.name] = item.value;
    });

    this.fightersDetailsMap.set(id, fighter);

  }
}

export default FightersView;