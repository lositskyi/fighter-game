import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const endpoint = `details/fighter/${_id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  // async setFighterDetails(_id, body) {
  //   try {
  //     const endpoint = `details/fighter/${_id}.json`;
  //     const apiResult = await callApi(endpoint, 'POST', body);

  //     return apiResult;
  //   } catch (error) {
  //     throw error;
  //   }
  // }
}

export const fighterService = new FighterService();
