import View from './view';

class DetailsView extends View {
  constructor(fighter, edit) {
    super();

    this.createDetails(fighter, edit);
  }

  createDetails(fighter, edit) {
    const { _id, name, health, attack, defense } = fighter;
    const nameElement = this.createName(name);
    const healthElement = this.createInfo('health', health);
    const attackElement = this.createInfo('attack', attack);
    const defenseElement = this.createInfo('defense', defense);
    const exitButton = this.createExitButton();
    const editButton = this.createButton('Edit', edit);

    const attributes = {id: _id};
    this.element = this.createElement({ tagName: 'div', className: 'fighterDetails', attributes});
    this.element.append(nameElement, healthElement, attackElement, defenseElement, exitButton, editButton);
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'h2', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createExitButton() {
    const button = this.createElement({tagName: 'div', className: 'exit'});
    button.innerHTML = `<i class="fas fa-times-circle"></i>`;
    button.addEventListener('click', () => (button.parentNode.remove()), false);

    return button;
  }

  createButton (name, handler) {
    const button = this.createElement({tagName: 'div', className: 'button'});
    button.innerText = name;
    button.addEventListener('click', event => handler(event), false);

    return button;
  }

  createInfo(head, data) {
    const infoElement = this.createElement({tagName: 'span', className: head});
    infoElement.innerHTML = `${head[0].toUpperCase() + head.slice(1)}: <br>`;
    const attributes = {type: 'text', value: data, name: head};
    const input = this.createElement({
      tagName: 'input', 
      className: 'info',
      attributes
    });

    infoElement.append(input);

    return infoElement;
  }
}

export default DetailsView;