class Fighter {
	constructor(fighter) {
		this.fighter = fighter;
		this.health = fighter.health;
		this.attack = fighter.attack;
		this.defense = fighter.defense;
	}

	getHitPower(attack) {
		return attack * createRandom(1,2);
	}

	getBlockPower(defense) {
		return defense * createRandom(1,2);
	}

	createRandom(max, min) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}
}

export const fighter = new Fighter();